package com.mojay.userapi.models;

import jakarta.persistence.*;
import lombok.*;

@Entity
@Data
@Table(name = "refresh_tokens")
@AllArgsConstructor
@NoArgsConstructor
public class RefreshToken {
    @Id
    @NonNull
    @GeneratedValue(strategy = GenerationType.UUID)
    private String id;

    @NonNull
    @OneToOne(targetEntity = User.class)
    private User owner;


}
