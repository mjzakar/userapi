package com.mojay.userapi.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Entity
@Data
@RequiredArgsConstructor
@NoArgsConstructor
@Table(name = "users")
public class User {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.UUID)
    private String id;
    @Column(name = "firstName")
    @NonNull
    private String firstName;
    @Column(name = "lastName")
    @NonNull
    private String lastName;
    @Column(name = "email")
    @NonNull
    private String email;
    @Column(name = "password")
    @NonNull
    @JsonIgnore
    private String password;
    @Column(name = "role")
    private String role;

}