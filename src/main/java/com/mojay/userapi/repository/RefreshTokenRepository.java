package com.mojay.userapi.repository;

import com.mojay.userapi.models.RefreshToken;
import com.mojay.userapi.models.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RefreshTokenRepository extends JpaRepository<RefreshToken, String> {
    boolean existsByOwner(User owner);
    Optional<RefreshToken> findByOwner(User owner);
}
