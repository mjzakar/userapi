package com.mojay.userapi.rest;

import com.mojay.userapi.dto.TokenDTO;
import com.mojay.userapi.jwt.JwtHelper;
import com.mojay.userapi.models.User;
import com.mojay.userapi.repository.RefreshTokenRepository;
import com.mojay.userapi.repository.UserRepository;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api/user")
public class UserREST {
    UserRepository userRepository;
    JwtHelper jwtHelper;
    UserREST(UserRepository userRepository, JwtHelper jwtHelper){
        this.userRepository = userRepository;
        this.jwtHelper = jwtHelper;
    }

    @PostMapping("/get")
    public ResponseEntity<?> getUserInfo( @RequestBody TokenDTO dto){
        Optional<User> user = userRepository.findById(dto.getUserId());
        if(user.isPresent()){
            if(jwtHelper.validateAccessToken(dto.getAccessToken())) return ResponseEntity.ok(user.get());
            else return ResponseEntity.status(401).build();
        }
        return ResponseEntity.status(404).build();
    }
}
