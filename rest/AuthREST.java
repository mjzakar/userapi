package com.mojay.userapi.rest;


import com.mojay.userapi.dto.LoginDTO;
import com.mojay.userapi.dto.SignupDTO;
import com.mojay.userapi.dto.TokenDTO;
import com.mojay.userapi.jwt.JwtHelper;
import com.mojay.userapi.models.RefreshToken;
import com.mojay.userapi.models.User;
import com.mojay.userapi.repository.RefreshTokenRepository;
import com.mojay.userapi.repository.UserRepository;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;


@RestController
@RequestMapping("/api/auth")
public class AuthREST {
    @Autowired
    RefreshTokenRepository refreshTokenRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    JwtHelper jwtHelper;

    @PostMapping("/login")
    @Transactional
    public ResponseEntity<?> login(@Valid @RequestBody LoginDTO dto) {
        Optional<User> userOptional = userRepository.findByEmail(dto.getEmail());
        if(userOptional.isPresent()){
            User user = userOptional.get();
            RefreshToken refreshToken;
            if(refreshTokenRepository.existsByOwner(user)) refreshToken = refreshTokenRepository.findByOwner(user).get();
            else  {
                refreshToken = new RefreshToken();
                refreshToken.setOwner(user);
            }
            refreshTokenRepository.save(refreshToken);
            String accessToken = jwtHelper.generateAccessToken(user);
            String refreshTokenString = jwtHelper.generateRefreshToken(user, refreshToken);
            return ResponseEntity.ok(new TokenDTO(user.getId(), accessToken, refreshTokenString));
        }
        else {
            return ResponseEntity.status(401).build();
        }
    }

    @PostMapping("signup")
    @Transactional
    public ResponseEntity<?> signup(@Valid @RequestBody SignupDTO dto) {
        User user = new User(dto.getFirstName(), dto.getLastName(), dto.getEmail(), dto.getPassword());
        Optional<User> duplicateEmail = userRepository.findByEmail(dto.getEmail());
        if(duplicateEmail.isPresent()) return ResponseEntity.status(409).build();
        else {
            userRepository.save(user);
            RefreshToken refreshToken;
            if(refreshTokenRepository.existsByOwner(user)) refreshToken = refreshTokenRepository.findByOwner(user).get();
            else  {
                refreshToken = new RefreshToken();
                refreshToken.setOwner(user);
            }
            refreshTokenRepository.save(refreshToken);

            String accessToken = jwtHelper.generateAccessToken(user);
            String refreshTokenString = jwtHelper.generateRefreshToken(user, refreshToken);

            return ResponseEntity.ok(new TokenDTO(user.getId(), accessToken, refreshTokenString));
        }
    }

    @PostMapping("logout")
    public ResponseEntity<?> logout(@RequestBody TokenDTO dto) {
        String refreshTokenString = dto.getRefreshToken();
        if (jwtHelper.validateRefreshToken(refreshTokenString) && refreshTokenRepository.existsById(jwtHelper.getTokenIdFromRefreshToken(refreshTokenString))) {
            // valid and exists in db
            refreshTokenRepository.deleteById(jwtHelper.getTokenIdFromRefreshToken(refreshTokenString));
            return ResponseEntity.ok().build();
        }
        else return ResponseEntity.status(403).build();
    }





    @PostMapping("refresh")
    public ResponseEntity<?> refreshToken(@RequestBody TokenDTO dto) {
        String refreshTokenString = dto.getRefreshToken();
        if (jwtHelper.validateRefreshToken(refreshTokenString) && refreshTokenRepository.existsById(jwtHelper.getTokenIdFromRefreshToken(refreshTokenString))) {
            // valid and exists in db

            refreshTokenRepository.deleteById(jwtHelper.getTokenIdFromRefreshToken(refreshTokenString));

            Optional<User> user = userRepository.findById(jwtHelper.getUserIdFromRefreshToken(refreshTokenString));

           if(user.isPresent()){
               RefreshToken refreshToken = new RefreshToken();
               refreshToken.setOwner(user.get());
               refreshTokenRepository.save(refreshToken);

               String accessToken = jwtHelper.generateAccessToken(user.get());
               String newRefreshTokenString = jwtHelper.generateRefreshToken(user.get(), refreshToken);

               return ResponseEntity.ok(new TokenDTO(user.get().getId(), accessToken, newRefreshTokenString));
           } else return ResponseEntity.status(404).build();
        }

        else return ResponseEntity.status(401).build();

    }
}
